package sample;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class FileAccessSample {

    public static void main(String[] args) throws IOException {
        fileREad();
    }

    private static void fileREad() throws IOException {
        Path path = Paths.get("C:\\Users\\jdquilop\\eclipse-workspace\\JavaExercises\\src\\sample\\TextFile.txt");
        Path newPath = Paths.get("C:/", "Users", "rapasive", "myNewFile.txt");
        
        Files.deleteIfExists(newPath);
        
        try (BufferedReader reader = Files.newBufferedReader(path);
                BufferedWriter writer = Files.newBufferedWriter(newPath, StandardOpenOption.APPEND, StandardOpenOption.CREATE)) {

            String str = null;

            while ((str = reader.readLine()) != null) {

                System.out.println(str);
                
                writer.newLine();
                writer.write(str);
//              writer.append(str);
                writer.newLine();
            }

        } catch (IOException e) {
            System.err.println("File not found!");
            e.printStackTrace();
        }

    }

    public static void paths() {
        Path path = Paths.get("C:/", "Users", "rapasive", "myFile.txt");
        Path path1 = Paths.get("/Users\\rapasive\\anotherDir");

        Path backPath = Paths.get("C:\\Users\\rapasive\\anotherDir\\..");

        // System.out.println("filename is "+ path1.getFileName());
        // System.out.println("parent is "+ path1.getParent());
        // System.out.println(backPath.normalize());
        // System.out.println("my tab \\ is tab");

        Path path2 = Paths.get("/Users/rapasive/targetFolder/target");

        // System.out.println(path1.relativize(path2));
        // System.out.println(path.getName(1));
        // System.out.println(path.getRoot());
        System.out.println(Files.exists(path, LinkOption.NOFOLLOW_LINKS));
    }

    
}

class MyReader implements AutoCloseable {

    @Override
    public void close() throws Exception {
        System.out.println("closed");
        
    }
    
}


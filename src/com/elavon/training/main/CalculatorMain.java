package com.elavon.training.main;

import com.elavon.training.impl.*;

public class CalculatorMain {

    public static void main(final String[] args) {
        final ImplementsCalculator tryMain = new ImplementsCalculator();
        System.out.println("Sum		:" + tryMain.getSum(8, 5));
        System.out.println("Difference	:" + tryMain.getDifference(8, 5));
        System.out.println("Product		:" + tryMain.getProduct(8, 5));
        System.out.println("Quotient	:" + tryMain.getQuotientAndRemainder(17, 5));
        System.out.println("Celcius		:" + tryMain.toCelsius(100));
        System.out.println("Fahrenheit	:" + tryMain.toFahrenheit(100));
        System.out.println("Kilo		:" + tryMain.toKilogram(100));
        System.out.println("Pound		:" + tryMain.toPound(100));
        System.out.println("Is Palidrome	:" + tryMain.isPalindrome("level"));
    }

}

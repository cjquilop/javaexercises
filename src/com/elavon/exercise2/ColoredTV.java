package com.elavon.exercise2;

public class ColoredTV extends ClassTV {

    protected int contrast = 50;

    protected int picture = 50;

    protected int brightness = 50;

    public ColoredTV(final String brand, final String model) {
        super(brand, model);

    }

    public void brightnessUp() {
        brightness++;
    }

    public void brightnessDown() {
        brightness--;
    }

    public void contrastUp() {
        contrast++;
    }

    public void contrastDown() {
        contrast--;

    }

    public void pictureUp() {
        picture++;

    }

    public void pictureDown() {
        picture--;

    }

    public int swtichToChannel(final int remotechannel) {
        return channel = remotechannel;
    }

    public int mute() {
        return 0;
    }

    @Override
    public String toString() {
        return super.toString() + "[ b:" + brightness + " || c:" + contrast + " || p:" + picture + "]";
    }

    public static void main(final String[] args) {
        final ColoredTV bnwTV = new ColoredTV("Admiral", "A1");
        final ColoredTV sonyTV = new ColoredTV("SONY", "S1");

        System.out.println(bnwTV);
        System.out.println(sonyTV);
        sonyTV.brightnessUp();
        
        final ColoredTV sharpTV = new ColoredTV("SHARP", "SH1");
        sharpTV.mute();
        sharpTV.brightnessDown();
        sharpTV.contrastDown();
        sharpTV.pictureDown();

        
        System.out.println(sharpTV);
    }
}

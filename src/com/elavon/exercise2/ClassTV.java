package com.elavon.exercise2;

public class ClassTV {

    protected String brand;

    protected String model;

    protected boolean powerOn;

    protected int channel;

    protected int volume;

    public ClassTV(final String brand, final String model) {
        this.brand = brand;
        this.model = model;
        powerOn = false;
        channel = 0;
        volume = 5;
    }

    public boolean turnOn() {
        return true;
    }

    public boolean turnOff() {
        return false;
    }

    public void channelUp() {
        channel++;
    }

    public void channelDown() {
        channel--;
    }

    public void volumeUp() {
        volume++;
    }

    public void volumeDown() {
        volume--;
    }

    @Override
    public String toString() {
        return brand + "[ on/off:" + powerOn + " || channel:" + channel + " || volume:" + volume + "]";
    }

    /*
     * public static void main(String args[]) { ClassTV TV1 = new ClassTV("Andre Electronics", "ONE");
     * System.out.println(TV1.toString()); TV1.turnOn(); for (int chan = 0; chan < 5; chan++) { TV1.channelUp(); }
     * TV1.channelDown(); for (int vol = 3; vol > 0; vol--) { TV1.volumeDown(); } TV1.volumeUp(); TV1.turnOff();
     * System.out.println(TV1.toString()); }
     */
}

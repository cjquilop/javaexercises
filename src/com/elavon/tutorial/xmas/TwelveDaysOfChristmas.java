package com.elavon.tutorial.xmas;

public class TwelveDaysOfChristmas {

    public static void printLyrics() {
        final int lastDay = 12;
        // String gift = "";
        final StringBuilder sb = new StringBuilder();
        for (int i = 1; i <= lastDay; i++) {

            switch (i) {
                case 1:
                    sb.append("a Partridge in a Pear Tree\n");
                    break;
                case 2:
                    sb.append("Two Turtle Doves\n");
                    break;
                case 3:
                    sb.append("Three French Hens\n");
                    break;
                case 4:
                    sb.append("Four Calling Birds\n");
                    break;
                case 5:
                    sb.append("Five Gold Rings\n");
                    break;
                case 6:
                    sb.append("Six Geese a-Laying\n");
                    break;
                case 7:
                    sb.append("Seven Swans a-Swimming\n");
                    break;
                case 8:
                    sb.append("Eight Maids a-Milking\n");
                    break;
                case 9:
                    sb.append("Nine Ladies Dancing\n");
                    break;
                case 10:
                    sb.append("Ten Lords a-Leaping\n");
                    break;
                case 11:
                    sb.append("Eleven Pipers Piping\n");
                    break;
                case 12:
                    sb.append("Twelve Drummers Drumming\n");
                    break;
            }

            System.out.println("On the " + i + " day of Christmas my true love sent to me " + sb.toString());

        }
    }

}

package com.elavon.exercise1;

public class Exercise1 {

    public static void main(final String[] args) {
        System.out.println("    ****       *      ******   *           *");
        System.out.println("   *          * *     *     *  *          * *");
        System.out.println("   *         *   *    ******   *         *   *");
        System.out.println("   *        *******   *   *    *        *******");
        System.out.println("    ****   *       *  *    *   *****   *       *");
        System.out.println(" ");
        System.out.println("    *    ***    *   *  *   *  *****   ****       *");
        System.out.println("    *   *   *   *   *  **  *    *    *          * *");
        System.out.println("    *   *   *   *****  * * *    *    *         *   *");
        System.out.println("*   *   *   *   *   *  *  **    *    *        *******");
        System.out.println(" ***     ***    *   *  *   *  *****   ****   *       *");
        System.out.println("  ");
        System.out.println("    ***    *   *  *****   *      ****    *****");
        System.out.println("   *   *   *   *    *     *     *    *   *    *");
        System.out.println("   * * *   *   *    *     *     *    *   *****");
        System.out.println("   *  **   *   *    *     *     *    *   * ");
        System.out.println("    *****   ***   *****   *****  ****    *       ");

    }

}

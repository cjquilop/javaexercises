package com.elavon.exercise5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArrayCollectionsExercise {
    public static void main(final String[] args) {
        final String[] countryArray = { "Korea", "Singapore", "Japan", "Spain", "Australia", "Switzerland", "France",
            "Thailand", "Vietnam", "USA" };

        System.out.println("Array Size :			" + countryArray.length);

        List<String> c;

        c = Arrays.asList(countryArray);
        c = new ArrayList<>(c);
        System.out.println("List Size :			" + c.size());
        c.add("Scotland");
        System.out.println("Is Array Empty : 		" + c.isEmpty());
        System.out.println("New List Size :			" + c.size());
        System.out.println("Is Mongolia in the List? : 	" + c.contains("Mongolia"));
        System.out.println("Is Mongolia in the List? : 	" + c.contains("Scotland"));
        c.remove(10);

        final List<String> top5 = new ArrayList<>(Arrays.asList("Indonesia", "United Kingdom", "France", "Italy",
            "USA"));

        for (final String string : countryArray) {
            if (top5.contains(string)) {
                System.out.println("This country is also in my list: " + string);
            }
        }

        System.out.println(c);

        final Map<Integer, String> aMap = new HashMap<>();

        for (int i = 1; i < c.size(); i++) {
            aMap.put(i, c.get(i));
        }

        Collections.sort(c);
        System.out.println(c);

        for (final Integer key : aMap.keySet()) {
            final String value = aMap.get(key);
            System.out.println("Rank #" + key + " : " + value);
        }
    }

}

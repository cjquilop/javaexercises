package com.elavon.exercise6;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JOptionPane;

public class Exercise6Main {

    public static void main(String[] args) {

        String options[] = { "Create Message", "Read Message" };

        String fileName = "C:\\Users\\jdquilop\\eclipse-workspace\\JavaExercises\\src\\com\\elavon\\exercise6\\temp.txt";

        StringBuilder sb = new StringBuilder();
        File file = new File(fileName);

        if (JOptionPane.showOptionDialog(null, "Choose below option", "Click a button", JOptionPane.DEFAULT_OPTION,
            JOptionPane.INFORMATION_MESSAGE, null, options, options[0]) == 0) {
            String username = JOptionPane.showInputDialog("Username");
            String recipient = JOptionPane.showInputDialog("Recipient Name");
            String message = JOptionPane.showInputDialog("Message");

            String data = username + " : " + recipient + " : " + message;
            // JOptionPane.showMessageDialog(null, data);

            sb = readFile(fileName);
            try (FileWriter fileWriter = new FileWriter(file.getAbsolutePath());
                 BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
                sb.append(data);
                // sb.append("\n");
                bufferedWriter.write(sb.toString());
                bufferedWriter.close();

            }
            catch (IOException ex) {
                System.out.println("Error writing to file '" + fileName + "'");
                // Or we could just do this:
                // ex.printStackTrace();
            }
        }
        else {
            // TODO

            JOptionPane.showMessageDialog(null, readFile(fileName));
        }

    }

    public static StringBuilder readFile(String fileName) {
        StringBuilder message = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(new File(fileName).getAbsolutePath()))) {

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                message.append(sCurrentLine);
                message.append("\n");
            }

        }
        catch (IOException e) {

            e.printStackTrace();

        }
        return message;
    }

}
